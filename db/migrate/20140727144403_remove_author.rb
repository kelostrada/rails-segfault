class RemoveAuthor < ActiveRecord::Migration
  def change
    remove_column :questions, :author
  end
end
