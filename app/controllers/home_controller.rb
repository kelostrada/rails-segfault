class HomeController < ApplicationController

  def index

    @current_time = Time.current

  end

  def current_time
    render plain: Time.current
  end

end
