class Question < ActiveRecord::Base

  belongs_to :user

  AUTHOR_FORMAT = /\A.+\z/

  validates :body,
            presence: true,
            length: {minimum: 3}
  validates :author,
            presence: true,
            format: {with: AUTHOR_FORMAT}

  def author_initials
    author.split.map(&:first).join.upcase
  end

  def author
    user.email
  end
end
