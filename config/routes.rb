Rails.application.routes.draw do

  devise_for :users
  resources :questions

  root 'home#index'

  get 'current_time' => 'home#current_time'

end
